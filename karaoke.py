import sys
from xml.sax import make_parser
import smallsmilhandler
import json
import urllib.request


class karaokeLocal:

    def __init__(self):
        self.fichero = fichero
        parser = make_parser()
        cHandler = smallsmilhandler.SmallSMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(self.fichero))
        self.lista = cHandler.get_tags()

    def __str__(self):
        texto = ''
        for cosas in self.lista:
            for etiqueta in cosas:
                if etiqueta == "etiqueta":
                    texto += cosas[etiqueta] + '\t'
                elif cosas[etiqueta] != "":
                    texto += etiqueta + '="' + cosas[etiqueta] + '"' + '\t'
            texto += '\n'
        return texto

    def to_json(self):
        with open(self.fichero.split(".")[0] + ".json", 'w') as f:
            json.dump(self.lista, f, indent=2)

    def do_local(self):
        for elementos in self.lista:
            for clave, valor in elementos.items():
                if clave == 'src':
                    val = valor.split('://')
                    if val[0] == 'http':
                        lugar = val[1].split('/')
                        urllib.request.urlretrieve(valor, lugar[-1])
                        elementos[clave] = lugar[-1]


if __name__ == "__main__":
    try:
        fichero = sys.argv[1]
    except FileNotFoundError:
        print("Usage:python3 karaoke.py file.smil")

    etiquetas = karaokeLocal()
    etiquetas.do_local()
    etiquetas.to_json()
    print(etiquetas)
