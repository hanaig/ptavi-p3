#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):
    """
    Clase para manejar chistes malos
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        super().__init__()
        self.width = ""
        self.height = ""
        self.backgroundcolor = ''
        self.id = ""
        self.top = ""
        self.bottom = ""
        self.left = ""
        self.right = ""
        self.src = ""
        self.region = ""
        self.begin = ""
        self.dur = ""
        self.region = ""
        self.img = ""
        self.audio = ""
        self.textstream = ""
        self.lista = []

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """

        if name == 'root-layout':
            self.width = attrs.get('width', "")
            self.height = attrs.get('height', "")
            self.backgroundcolor = attrs.get('background-color', "")
            listaRootLay = {'etiqueta': name, 'width': self.width,
                            'height': self.height,
                            'background-color': self.backgroundcolor}
            self.lista.append(listaRootLay)

        elif name == 'region':
            self.id = attrs.get('id', "")
            self.top = attrs.get('top', "")
            self.left = attrs.get('left', "")
            self.bottom = attrs.get('bottom', "")
            self.right = attrs.get('right', "")
            listaRegion = {'etiqueta': name, 'id': self.id, 'top': self.top,
                           'left': self.left, 'bottom': self.bottom,
                           'right': self.right}
            self.lista.append(listaRegion)

        elif name == 'img':
            self.src = attrs.get('src', "")
            self.begin = attrs.get('begin', "")
            self.dur = attrs.get('dur', "")
            self.region = attrs.get('region', "")
            listaImg = {'etiqueta': name, 'src': self.src, 'begin': self.begin,
                        'dur': self.dur, 'region': self.region}
            self.lista.append(listaImg)

        elif name == 'audio':
            self.src = attrs.get('src', "")
            self.begin = attrs.get('begin', "")
            self.dur = attrs.get('dur', "")
            listaAudio = {'etiqueta': name, 'src': self.src,
                          'begin': self.begin, 'dur': self.dur}
            self.lista.append(listaAudio)

        elif name == 'textstream':
            self.src = attrs.get('src', "")
            self.region = attrs.get('region', "")
            listaTextstream = {'etiqueta': name, 'dur': self.src,
                               'region': self.region}
            self.lista.append(listaTextstream)

    def get_tags(self):
        return self.lista


if __name__ == "__main__":
    """
    Programa principal
    """
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    lista = cHandler.get_tags()
    print(lista)
